//Morteza Mohandes Mojarrad
//student number: 9523115
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <string>
int main () {
  int c{1}, min{}, index{} , temp{};
  std::vector<int> v{};

  // getting inputs till 0
  while (true)
    {
      std::cin >> c;
      if(!c)
	break;
    v.push_back(c);
    }

  // selection sorting
  for (size_t i{} ; i < v.size()-1 ; i++)
    {
     index=i;
     min=v[i];
     for(size_t j{i} ; j < v.size()-1 ; j++)
       {
       if(min >= v[j+1])
	 {
	   min = v[j+1];
	   index = j+1;
	 }
      
       }
     //swap min 
     temp = v[i];
     v[i]=min;
     v[index]=temp;
    }
  //display sorted vector
   for(size_t i{} ; i < v.size() ; i++)
     std::cout << v[i] << std::endl;
   
  return 0;
}

	 
		
