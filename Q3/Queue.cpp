//Morteza Mohandes Mojarrad
//student number: 9523115
#include "Queue.h"

Queue::Queue(std::string s){
  char ch{};
  front = 0;
  std::ifstream iff(s);
  iff >> size;
  arr = new double[size];
    
  while(rear++ < size){
    //reaches end of file : breaks
    if (iff.eof())
      {
	rear--;
	break;
      }
    // put number to arr
    iff >> arr[rear];
    //flush charaacter ',' out
    iff >> ch;
      }

}


void Queue::enQueue(double x){

      if ((front == 0 && rear == size-1) ||
            (rear == front-1))
    {
      std::cout << "Queue is Full" << std::endl;
        return;
    }
 
    else if (front == -1) /* Insert First Element */
    {
        front = rear = 0;
        arr[rear] = x;
    }
 
    else if (rear == size-1 && front != 0)
    {
        rear = 0;
        arr[rear] = x;
    }
 
    else
    {
        rear++;
        arr[rear] = x;
    }

}

int Queue::deQueue(){
  
  if (front == -1)
    {
      std::cout <<  "Queue is Empty" << std::endl;
        return -1;
    }
 
    int data = arr[front];
    arr[front] = -1;
    if (front == rear)
    {
        front = -1;
        rear = -1;
    }
    else if (front == size-1)
        front = 0;
    else
        front++;
 
    return data;
  

}

void Queue::displayQueue(){

  if (front == -1)
    {
      std::cout << "Queue is Empty.";
      
        return;
    }
  std::cout << "Elements in Circular Queue are: " << std::endl;
    if (rear >= front)
    {
        for (int i = front; i <= rear; i++)
	  std::cout << arr[i] << std::endl;
	  
    }
    else
    {
        for (int i = front; i < size; i++)
	  std::cout << arr[i] << std::endl;
 
        for (int i = 0; i <= rear; i++)
	  std::cout << arr[i] << std::endl;
    }

}
