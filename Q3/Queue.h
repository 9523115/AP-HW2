#include <fstream>
#include <string>
#include <iostream>
class Queue{
 public:
  Queue(std::string s);
  void enQueue(double);
  int deQueue();
  void displayQueue();
 private:
 
  int rear{-1} , front{-1} , size{};
  double* arr{};
  
};
